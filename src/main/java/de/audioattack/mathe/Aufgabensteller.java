package de.audioattack.mathe;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.regex.Pattern;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class Aufgabensteller {

    private static final int OBERGRENZE = 100;

    private final JTextField tfAufgabe;
    private final JTextField taLösung;
    private final JTextField tfStatus;

    private int fehler = 0;

    private Aufgabensteller() {
        tfAufgabe = new JTextField();
        tfAufgabe.setHorizontalAlignment(SwingConstants.CENTER);
        tfAufgabe.setFocusable(false);

        taLösung = new JTextField(10);
        taLösung.setHorizontalAlignment(SwingConstants.CENTER);
        ((AbstractDocument) taLösung.getDocument()).setDocumentFilter(new DocumentFilter() {
            final Pattern regEx = Pattern.compile("\\d*");

            @Override
            public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
                if (text != null && !regEx.matcher(text).matches()
                        || taLösung.getText() != null && taLösung.getText().length() > (Math.log10(OBERGRENZE) + 1)
                ) {
                    return;
                }
                super.replace(fb, offset, length, text, attrs);
            }
        });

        tfStatus = new JTextField("Hallo!");
        tfStatus.setHorizontalAlignment(SwingConstants.CENTER);
        tfStatus.setFocusable(false);

        JFrame jFrame = new JFrame("Rechno");
        jFrame.setSize(640, 480);
        GridLayout gridLayout = new GridLayout(0, 2);
        jFrame.setLayout(gridLayout);
        jFrame.getContentPane().add(tfAufgabe);
        jFrame.getContentPane().add(taLösung);

        jFrame.getContentPane().add(tfStatus);

        jFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        //jFrame.pack();
        jFrame.setVisible(true);

    }


    public static void main(String[] args) {

        final Aufgabensteller aufgabensteller = new Aufgabensteller();
        aufgabensteller.stelleAufgabe(aufgabensteller.next());
    }

    private Aufgabe next() {

        return new AufgabenAusdenker(OBERGRENZE).erzeugeZufällig();
    }

    private void stelleAufgabe(final Aufgabe aufgabe) {

        final long zeitAnfang = System.currentTimeMillis();

        tfAufgabe.setText(aufgabe.toString());

        taLösung.requestFocus();

        taLösung.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                String eingabe = taLösung.getText();
                tfStatus.setText(null);
                tfStatus.setForeground(Color.BLACK);

                if (eingabe != null && eingabe.length() > 0 && keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {

                    long zeitEnde = System.currentTimeMillis();

                    boolean richtig = aufgabe.istKorrekt(Integer.parseInt(eingabe));

                    taLösung.setText(null);

                    if (richtig) {
                        tfStatus.setText("Richtig!");
                        fehler = 0;
                        taLösung.removeKeyListener(this); // TODO
                        stelleAufgabe(next());
                    } else {
                        fehler++;
                        tfStatus.setForeground(Color.RED);
                        tfStatus.setText(eingabe + " ist leider falsch! \uD83D\uDE1F");

                        if (fehler > 2) {
                            taLösung.removeKeyListener(this); // TODO
                            stelleAufgabe(next());
                            fehler = 0;
                        }
                    }

                    final float zeitVergangen = (zeitEnde - zeitAnfang) / 1000f;

                    System.out.println("Du hast " + zeitVergangen + " Sekunden benötigt.");
                }
            }
        });
    }


}
