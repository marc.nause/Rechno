package de.audioattack.mathe;

public enum Operation {

    ADDITION("+"), SUBTRAKTION("-"), DIVISION(":"), MULTIPLIKATION("*");

    private final String zeichen;

    Operation(final String zeichen) {
        this.zeichen = zeichen;
    }

    @Override
    public String toString() {
        return zeichen;
    }
}
