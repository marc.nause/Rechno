package de.audioattack.mathe;

import java.util.Random;

public class AufgabenAusdenker {

    private final Random random = new Random();

    private final int obergrenze;

    public AufgabenAusdenker(final int obergrenze) {
        this.obergrenze = obergrenze;
    }

    public Aufgabe erzeugeZufällig() {

        final float zufall = random.nextFloat();

        if (zufall < 0.25) {
            return erzeugeAddition();
        } else if (zufall < 0.5) {
            return erzeugeDivision();
        } else if (zufall < 0.75) {
            return erzeugeMultiplikation();
        } else {
            return erzeugeSubtraktion();
        }

    }

    public Aufgabe erzeugeAddition() {

        final int ergebnis = random.nextInt(obergrenze);
        final int zahl1 = random.nextInt(ergebnis - 1);
        final int zahl2 = ergebnis - zahl1;
        return new Aufgabe(zahl1, zahl2, ergebnis, Operation.ADDITION);
    }

    public Aufgabe erzeugeSubtraktion() {

        final int zahl1 = random.nextInt(obergrenze);
        final int zahl2 = random.nextInt(zahl1 - 1);
        final int ergebnis = zahl1 - zahl2;
        return new Aufgabe(zahl1, zahl2, ergebnis, Operation.SUBTRAKTION);
    }

    public Aufgabe erzeugeMultiplikation() {

        final int zahl1 = Math.max(2, random.nextInt((int) Math.round(Math.sqrt(obergrenze))));
        final int zahl2 = Math.max(2, random.nextInt((int) Math.round(Math.sqrt(obergrenze))));
        return new Aufgabe(zahl1, zahl2, zahl1 * zahl2, Operation.MULTIPLIKATION);
    }

    public Aufgabe erzeugeDivision() {

        final int zahl1 = Math.max(2, random.nextInt(obergrenze));
        final int zahl2 = Math.max(2, random.nextInt(zahl1 / 2));
        final int ergebnis = zahl1 / zahl2;
        return new Aufgabe(ergebnis * zahl2, zahl2, ergebnis, Operation.DIVISION);
    }

    // kleiner

    // größer

    // gleich

    // Blitzaufgabe

    // Zahlwort für z.B. 93 -> Dreiundneunzig

}
