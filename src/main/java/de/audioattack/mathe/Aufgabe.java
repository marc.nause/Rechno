package de.audioattack.mathe;

public class Aufgabe {

    private final int ergebnis;
    private final Operation operation;
    private final int zahl1;
    private final int zahl2;

    public Aufgabe(final int zahl1, final int zahl2, final int ergebnis, final Operation operation) {
        this.ergebnis = ergebnis;
        this.zahl1 = zahl1;
        this.zahl2 = zahl2;
        this.operation = operation;
    }

    public boolean istKorrekt(final int zahl) {
        return ergebnis == zahl;
    }

    public String toString() {
        return zahl1 + " " + operation + " " + zahl2 + " = ";
    }

    public int gibErgebnis() {
        return ergebnis;
    }
}
